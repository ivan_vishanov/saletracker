"use strict";

function getCampaignsListingData() {
    const PromotionMgr = require("dw/campaign/PromotionMgr");
    let campaigns = PromotionMgr.getCampaigns();
    let data = [];

    campaigns.toArray().forEach(function (campaign) {
        data.push({
            id: campaign.ID,
            description: campaign.description,
            manager: session.userName,
        });
    });

    return data;
}

function getPromotionModel(promotion, order) {
    const Promotion = require("dw/campaign/Promotion");
    const promotionProducts = [];
    const promotionClass = promotion.promotionClass;
    let isApplied;

    if (promotionClass.equals(Promotion.PROMOTION_CLASS_SHIPPING)) {
        isApplied = !empty(order.getShippingPriceAdjustmentByPromotionID(promotion.ID));
    } else if (promotionClass.equals(Promotion.PROMOTION_CLASS_ORDER)) {
        isApplied = !empty(order.getPriceAdjustmentByPromotionID(promotion.ID));
    } else {
        const productLineItems = order.productLineItems;

        if (!empty(productLineItems)) {
            productLineItems.toArray().forEach(function (item) {
                if (!empty(item.getPriceAdjustmentsByPromotionID(promotion.ID))) {
                    isApplied = true;

                    promotionProducts.push({
                        lineItemUUID: item.getUUID(),
                        id: item.productID,
                        price: item.adjustedGrossPrice,
                        quantity: item.getQuantityValue(),
                    });
                }
            });
        }
    }

    return {
        isApplied: isApplied,
        productLineItems: promotionProducts,
    };
}

function getCampaignDetails(campaign) {
    const OrderMgr = require("dw/order/OrderMgr");
    const Order = require("dw/order/Order");
    const HashMap = require("dw/util/HashMap");

    let promotions = campaign.promotions.toArray();
    let orders = OrderMgr.queryOrders("status != {0} AND status != {1}", null, Order.ORDER_STATUS_CANCELLED, Order.ORDER_STATUS_FAILED).asList().toArray();

    const campaignCustomers = new HashMap();
    const revenueOrders = [];
    const campaignProducts = new HashMap();
    let revenue;

    promotions.forEach(function (promotion) {
        orders.forEach(function (order) {
            let promotionModel = getPromotionModel(promotion, order);
            if (promotionModel.isApplied) {
                if (revenueOrders.indexOf(order.orderNo) === -1) {
                    let orderPrice = order.adjustedMerchandizeTotalGrossPrice;
                    revenue = !empty(revenue) ? revenue.add(orderPrice) : orderPrice;

                    let customerEmail = order.getCustomerEmail();
                    let campaignCustomer = campaignCustomers.get(customerEmail);
                    var test = campaignCustomer;
                    if (empty(campaignCustomer)) {
                        campaignCustomer = {
                            email: customerEmail,
                            moneySpent: orderPrice,
                        };
                    } else {
                        campaignCustomer.moneySpent = campaignCustomer.moneySpent.add(orderPrice);
                    }
                    campaignCustomers.put(customerEmail, campaignCustomer);
                }
                promotionModel.productLineItems.forEach(function (product) {
                    if (!campaignProducts.containsKey(product.lineItemUUID)) {
                        campaignProducts.put(product.lineItemUUID, product);
                    }
                });
            }
        });
    });

    return {
        customers: campaignCustomers.values().toArray(),
        revenue: revenue,
        products: campaignProducts.values().toArray(),
    };
}

function getSummarizedProducts(campaignDetails) {
    let products = {};

    campaignDetails.products.forEach(function (product) {
        let summarizedProduct = products[product.Id];
        if (!empty(summarizedProduct)) {
            summarizedProduct.quantity += product.quantity;
            summarizedProduct.price.add(product.price);
        } else {
            products[product.id] = {
                quantity: product.quantity,
                price: product.price,
            };
        }
    });

    return products;
}

function getChartsProductInfo(summarizedProducts) {
    let productsIds = Object.keys(summarizedProducts);
    let quantities = [];
    let prices = [];

    productsIds.forEach(function (productId) {
        let product = summarizedProducts[productId];
        quantities.push(product.quantity);
        prices.push(product.price.value);
    });

    return {
        productsIds: productsIds,
        quantities: quantities,
        prices: prices,
    };
}

function getCampaignDetailedData(campaignId) {
    const PromotionMgr = require("dw/campaign/PromotionMgr");
    const Resource = require("dw/web/Resource");
    let campaign = PromotionMgr.getCampaign(campaignId);

    if (empty(campaign)) {
        return null;
    }

    let campaignDetails = getCampaignDetails(campaign);
    let summarizedProducts = getSummarizedProducts(campaignDetails);
    let chartsProductInfo = getChartsProductInfo(summarizedProducts);

    return {
        id: campaign.ID,
        description: campaign.description || "",
        startDate: campaign.startDate || Resource.msg("saletracker.campaign.continious.date", "saletracker", ""),
        endDate: campaign.endDate || Resource.msg("saletracker.campaign.continious.date", "saletracker", ""),
        manager: session.userName,
        revenue: campaignDetails.revenue,
        chartsProductInfo: chartsProductInfo,
        customers: campaignDetails.customers,
    };
}

module.exports = {
    getCampaignsListingData: getCampaignsListingData,
    getCampaignDetailedData: getCampaignDetailedData,
};
