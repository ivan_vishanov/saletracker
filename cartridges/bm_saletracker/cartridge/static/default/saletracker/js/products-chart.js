$(function() {
    "use strict";

    var ctx = document.getElementById("products-chart-js");
    var iframe = $(window.parent.document.getElementById('products-chart-iframe'));
    var products = iframe.data('chart') || {};
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: products.productsIds.length ? products.productsIds : ['NONE'],
            datasets: [{
                label: 'Total sold:',
                data: products.prices.length ? products.prices : [0],
                backgroundColor: "rgba(89, 105, 255,.8)",
                borderColor: "rgba(89, 105, 255,1)",
                borderWidth:2
            }]

        },
        options: {
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontColor: '#71748d',
                    fontFamily:'Circular Std Book',
                    fontSize: 14,
                }
            },

            scales: {
                xAxes: [{
                    ticks: {
                        fontSize: 14,
                        fontFamily:'Circular Std Book',
                        fontColor: '#71748d',
                    }
                }],
                yAxes: [{
                    ticks: {
                        fontSize: 14,
                        fontFamily:'Circular Std Book',
                        fontColor: '#71748d',
                        beginAtZero: true,
                        maxTicksLimit: 5
                    }
                }]
            }
        }
    });
});