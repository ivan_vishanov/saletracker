'use strict';

const server = require('server');
const saleTracker = require('~/cartridge/scripts/helpers/saleTracker');

server.get('Index', server.middleware.https, function (req, res, next) {
    let campaignsData = saleTracker.getCampaignsListingData();

    res.render('index', {
        campaigns: campaignsData
    });

    next();
});

server.get('Campaign', server.middleware.https, function (req, res, next) {
    let campaignModel = saleTracker.getCampaignDetailedData(req.httpParameterMap.get('id'));
    if (empty(campaignModel)) {
        res.render('campaignempty', {
            campaign: {id: req.httpParameterMap.get('id')}
        });
    } else {
        res.render('campaigndetails', {
            campaign: campaignModel
        });
    }

    next();
});

server.get('ProductsChart', server.middleware.https, function (req, res, next) {
    res.render('productschart');

    next();
});
module.exports = server.exports();
