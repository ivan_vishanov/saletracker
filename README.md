Sale Tracker:
Create an extension to the Business manager to reflect the following reports.
Consider There is a special sale  going inside the company.
The company wants to track down all the information and disclose it to the internal admin.
1. what was it about,
2. Person who leads the sales/ Channel manager
3. when was it (Blackfriday, winter clearance.. etc..)
4. who participated (Customers),
5. how much was the revenue

Also, the company wants to keep track of information about the products that were sold within Demandware. Design a dashboard showing stats of all the products sold. An admin can go and add the product to the system and the product quantity can be decreased from Database inventory as soon as it is added to orders.

The system should leverage multiple currencies based on sales in different countries.
In order to achieve the requirements if any integration needs to be added with an external system (i.e. google analytics) that is in scope.